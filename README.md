# Markdownify

Markdownify is a simple Single Page Application build with [ClojureScript](https://clojurescript.org/) and built with [shadowcljs](https://github.com/thheller/shadow-cljs)

You have two editors and an HTML preview. You can type markdown which is reactively converted to HTML and vice versa. At the the same time you can preview the changes as you edit either the HTML or Markdown.

The application is deployed on netlify. Visit [Markdown Editor](https://hungry-murdock-f5fcec.netlify.app/) for the DEMO.

# Licence

[MIT](https://opensource.org/licenses/MIT)
